package pl.kino.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jakub on 25.05.17.
 */
@Entity
@NamedQueries({})
@NamedQuery(name="Movie.findLastThreeDateDesc", query = "Select m From Movie m ORDER BY m.id DESC")
public class Movie implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="movie_id")
    private Long id;

    @Column(nullable = false, length = 255)
    private String title;

    @Column(nullable = false, length = 10000)
    private String description;

    @Column(length = 1020)
    private String imgUrl;

    public Long getId() {
        return id;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
