package pl.kino.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by jakub on 25.05.17.
 */
@Entity
@NamedQueries({
        @NamedQuery(name="Ticket.findAllByUser", query = "Select t From Ticket t where t.user=:user"),
        @NamedQuery(name="Ticket.findCurrentByUser", query = "Select t From Ticket t where t.user=:user and t.date>=:currentDate")
})
public class Ticket implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ticket_id")
    private Long id;

    /*
    Price in "grosz".
     */
    @Column(nullable = false)
    private int price;

    @Column(nullable = false)
    private TicketType type;

    @Column(nullable = false)
    private int seat;

    @Column(nullable = false)
    private int row;

    @Column(nullable = false)
    private Date date;

    @ManyToOne
    private Showing showing;

    @OneToOne
    private User user;

    public Ticket(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public TicketType getType() {
        return type;
    }

    public void setType(TicketType type) {
        this.type = type;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Showing getShowing() {
        return showing;
    }

    public void setShowing(Showing showing) {
        this.showing = showing;
        this.date = showing.getDate();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
