package pl.kino.model;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by jakub on 26.05.17.
 */
@Entity
public class Screen implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="screen_id")
    private Long id;

    @Column(unique = true, nullable = false)
    private int screenNumer;

    /*
    Seats - horizontaly
    Row - verticaly
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Seat> seats = new ArrayList<>();


    public Long getId() {
        return id;
    }

    public int getScreenNumer() {
        return screenNumer;
    }

    public void setScreenNumer(int screenNumer) {
        this.screenNumer = screenNumer;
    }

    public void addSeat(Seat seat){
        this.seats.add(seat);
    }
    public List<Seat> getSeats() {
        return seats;
    }

    public ArrayList<Seat> getSeatsCode() {
        ArrayList<Seat> seatsR = new ArrayList<>();
        Collections.copy(this.seats, seatsR);
        for(Seat s : seatsR)
            s.setId(null);
        return seatsR;
    }
    public void setSeats(ArrayList<Seat> seats) {
        this.seats = seats;
    }
}
