package pl.kino.model;

import javax.persistence.*;

/**
 * Created by jakub on 12.06.17.
 */
@Entity
public class Seat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="seat_id")
    private Long id;

    private int nrOfRow;

    private int nrOfColumn;

    private int status;

    public Seat(){}

    public Seat(int row, int column) {
        this.nrOfRow=row;
        this.nrOfColumn = column;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getRow() {
        return nrOfRow;
    }

    public void setRow(int row) {
        this.nrOfRow = row;
    }

    public int getColumn() {
        return nrOfColumn;
    }

    public void setColumn(int column) {
        this.nrOfColumn= column;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
