package pl.kino.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jakub on 26.05.17.
 */
@Entity
public class TicketType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ticket_type_id")
    private Long id;

    @Column(nullable = false)
    private String type;

    @Column(nullable = false)
    private int price;

    public Long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }


}
