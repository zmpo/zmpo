package pl.kino.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import pl.kino.model.Movie;
import pl.kino.repository.MovieRepository;

import java.util.List;

/**
 * Created by jakub on 27.05.17.
 */
@Service
public class MovieService {

    private MovieRepository movRep;

    @Autowired
    public MovieService(MovieRepository movRep){
        this.movRep=movRep;
    }

    public void saveMovie(Movie mov){
        movRep.saveAndFlush(mov);
    }

    public Movie getOneById(Long id) {
        return  movRep.findOne(id);
    }
    public List<Movie> getAllMovies(){
        return movRep.findAll();
    }
    public List<Movie> getLastThreeMovies() {
        return (List<Movie>) movRep.findLastThreeDateDesc(new PageRequest(0,3));
    }


}
