package pl.kino.service;

import org.springframework.stereotype.Service;
import pl.kino.model.Screen;
import pl.kino.repository.ScreenRepository;

import java.util.List;

/**
 * Created by jakub on 11.06.17.
 */
@Service
public class ScreenService {

    private ScreenRepository scrRep;

    public ScreenService(ScreenRepository scrRep){
        this.scrRep=scrRep;
    }

    public void save(Screen screen){
        scrRep.saveAndFlush(screen);
    }

    public List<Screen> getAllScreens(){
        return scrRep.findAll();
    }

    public Screen findOneById(Long id){
        return scrRep.findOne(id);
    }
}
