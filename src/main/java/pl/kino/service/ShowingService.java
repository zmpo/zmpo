package pl.kino.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kino.model.Showing;
import pl.kino.model.Ticket;
import pl.kino.model.User;
import pl.kino.repository.ShowingRepository;
import pl.kino.repository.TicketRepository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

/**
 * Created by jakub on 26.05.17.
 */
@Service
public class ShowingService {

    private ShowingRepository showingRepository;
    private TicketRepository ticketRepository;

    @Autowired
    public ShowingService(ShowingRepository showingRepository,
                          TicketRepository ticketRepository){
        this.showingRepository=showingRepository;
        this.ticketRepository=ticketRepository;
    }

    public void addShowing(Showing showing){
        showingRepository.save(showing);
    }

    public void updateShowing(Showing showing){
        showingRepository.save(showing);
    }

    public List<Showing> getAllShowings(){
        return showingRepository.findAll();
    }
    public Showing getShowing(Long idShowing){
        return showingRepository.findOne(idShowing);
    }

    public void addTicket(Ticket ticket){
        ticketRepository.save(ticket);
    }

    public Ticket getTicket(Long ticketId){
        return ticketRepository.findOne(ticketId);
    }

    public List<Ticket> getAllTickets(User user){
        return ticketRepository.findAllByUser(user);
    }

    public List<Ticket> getTickets(User user){
        return ticketRepository.findCurrentByUser(user, new Date());
    }

}
