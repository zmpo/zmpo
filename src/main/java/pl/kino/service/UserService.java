package pl.kino.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pl.kino.model.User;
import pl.kino.model.UserRole;
import pl.kino.repository.UserRepository;
import pl.kino.repository.UserRoleRepository;

import java.util.List;

/**
 * Created by jakub on 26.05.17.
 */
@Service
public class UserService {

    private static final String DEFAULT_ROLE = "ROLE_USER";
    private UserRepository userRepository;
    private UserRoleRepository userRoleRepository;

    @Autowired
    public UserService(UserRepository userRepository, UserRoleRepository userRoleRepository){
        this.userRepository=userRepository;
        this.userRoleRepository=userRoleRepository;
    }
    public void addWithDefaultRole(User user) {
        UserRole defaultRole = userRoleRepository.findByRole(DEFAULT_ROLE);
        user.getRoles().add(defaultRole);
        userRepository.save(user);
    }

//    public User getUser(){
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        User user = userRepository.findByEmail(auth.getName());
//        return user;
//    }

    public List<User> findAllUsers(){
        return userRepository.findAll();
    }

    public User findUserById(Long id){
        return userRepository.findById(id);
    }

    public boolean isUserExist(User user){
        return (userRepository.findByEmail(user.getEmail()) == null && userRepository.findByUsername(user.getUsername())==null) ? true : false;
    }

    public void updateUser(User user){
        User u = findUserById(user.getId());
        u.setEmail(user.getEmail());
        u.setPassword(user.getPassword());
        u.setRoles(user.getRoles());
    }

    public void deleteUserById(Long userId){
        userRepository.delete(userId);
    }
}