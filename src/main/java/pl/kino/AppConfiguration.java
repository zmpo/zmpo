package pl.kino;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import pl.kino.model.User;
import pl.kino.repository.UserRepository;

/**
 * Created by jakub on 01.06.17.
 */
@Configuration
public class AppConfiguration {

    @Autowired
    private UserRepository userRepository;

    @Bean(name="loggedUser")
    public User loggedUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = null;
        if(auth!=null){
            user = userRepository.findByUsername(auth.getName());
        }
        return user;
    }
}
