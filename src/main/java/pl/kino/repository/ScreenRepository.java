package pl.kino.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kino.model.Screen;

/**
 * Created by jakub on 26.05.17.
 */
@Repository
public interface ScreenRepository extends JpaRepository<Screen, Long> {
}
