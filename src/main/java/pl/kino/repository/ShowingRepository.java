package pl.kino.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.kino.model.Showing;

/**
 * Created by jakub on 26.05.17.
 */
public interface ShowingRepository extends JpaRepository<Showing, Long> {
}
