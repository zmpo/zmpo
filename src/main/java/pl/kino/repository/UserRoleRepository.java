package pl.kino.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.kino.model.UserRole;

/**
 * Created by jakub on 12.02.17.
 */
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
    UserRole findByRole(String role);
}
