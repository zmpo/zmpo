package pl.kino.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.kino.model.TicketType;

/**
 * Created by jakub on 26.05.17.
 */
public interface TicketTypeRepository extends JpaRepository<TicketType, Long> {
}
