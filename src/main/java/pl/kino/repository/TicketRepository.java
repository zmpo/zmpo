package pl.kino.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.kino.model.Ticket;
import pl.kino.model.User;

import java.util.Date;
import java.util.List;

/**
 * Created by jakub on 26.05.17.
 */
@Repository
public interface TicketRepository extends JpaRepository<Ticket,Long> {

    @Query(name = "Ticket.findAllByUser")
    public List<Ticket> findAllByUser(@Param("user") User user);

    @Query(name = "Ticket.findCurrentByUser")
    public List<Ticket> findCurrentByUser(@Param("user") User user, @Param("currentDate") Date date);
}
