package pl.kino.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.kino.model.Movie;

import java.util.List;

/**
 * Created by jakub on 25.05.17.
 */
public interface MovieRepository extends JpaRepository<Movie,Long>{

    @Query(name="Movie.findLastThreeDateDesc")
    public List<Movie> findLastThreeDateDesc(Pageable pageable);

}