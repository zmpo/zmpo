package pl.kino.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.kino.model.Movie;
import pl.kino.service.MovieService;

import java.util.List;

/**
 * Created by jakub on 25.05.17.
 */
@Controller
public class HomeController {

    private MovieService movSrv;

    @Autowired
    public HomeController(MovieService movSrv){
        this.movSrv=movSrv;
    }
    @RequestMapping("/")
    public String home(Model model){
        model.addAttribute("last3Movies",movSrv.getLastThreeMovies());
        model.addAttribute("movie",new Movie());
        return "index";
    }


}
