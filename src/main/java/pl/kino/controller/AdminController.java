package pl.kino.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.kino.model.Movie;
import pl.kino.model.Screen;
import pl.kino.model.Seat;
import pl.kino.model.Showing;
import pl.kino.service.MovieService;
import pl.kino.service.ScreenService;
import pl.kino.service.SettingsService;
import pl.kino.service.ShowingService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by jakub on 08.06.17.
 */
@Controller
public class AdminController {

    private MovieService movSrv;
    private ScreenService scrSrv;
    private SettingsService settSrv;
    private ShowingService showSrv;

    @Autowired
    public AdminController(MovieService movSrv,
                           ScreenService scrSrv,
                           SettingsService settSrv,
                           ShowingService showSrv){
        this.movSrv=movSrv;
        this.scrSrv=scrSrv;
        this.settSrv=settSrv;
        this.showSrv=showSrv;
    }

    @RequestMapping("/admin")
    public String index(){
        return "admin/index";
    }

    @RequestMapping("/admin/movie")
    public String movie(Model model){
        model.addAttribute("movie",new Movie());
        List<Movie> mL= movSrv.getAllMovies();
        model.addAttribute("movieList",movSrv.getAllMovies());
        return "admin/movie";
    }

    @RequestMapping("admin/movie/add")
    public String movieAdd(@ModelAttribute Movie movie,
                           BindingResult bindingResult,
                           Model model){
        if(!bindingResult.hasErrors()){
            movSrv.saveMovie(movie);
        } else {
            //TODO BLAD
        }
        return "redirect:/admin/movie";
    }

    @RequestMapping("/admin/screen")
    public String screen(Model model){
        model.addAttribute("screen",new Screen());
        model.addAttribute("screenList",scrSrv.getAllScreens());
        return "admin/screen";
    }

    @RequestMapping("admin/screen/add")
    public String movieAdd(@RequestParam("screenNumber") int screenNumber,
                           @RequestParam("seatsInRow") int seatsInRow,
                           @RequestParam("seatsInColumn") int seatsInColumn,
                           Model model){
        Screen screen = new Screen();
        screen.setScreenNumer(screenNumber);
        for(int i=0; i<seatsInRow; i++)
            for(int j=0; j<seatsInColumn; j++)
                screen.getSeats().add(new Seat(i,j));
        scrSrv.save(screen);
        return "redirect:/admin/screen";
    }

    @RequestMapping("/admin/showing")
    public String showing(Model model){
        model.addAttribute("screen",new Showing());
        model.addAttribute("movieList",movSrv.getAllMovies());
        model.addAttribute("screenList",scrSrv.getAllScreens());
        model.addAttribute("date", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()).toString());
        model.addAttribute("showingList",showSrv.getAllShowings());

        return "admin/showing";
    }

    @RequestMapping("admin/showing/add")
    public String ShowingAdd(@RequestParam("screenId") Long screenId,
                           @RequestParam("movieId") Long movieId,
                           @RequestParam("seatsInColumn") String dateForm,
                           Model model) throws ParseException {
        Showing showing = new Showing();
        showing.setMovie(movSrv.getOneById(movieId));
        showing.setScreen(scrSrv.findOneById(screenId));
        showing.setDate(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(dateForm));

        showSrv.addShowing(showing);

        return "redirect:/admin/showing";
    }

    @RequestMapping("/admin/settings")
    public String settings(Model model){
        model.addAttribute("EXTRA_PRICE",settSrv.getValue("EXTRA_PRICE"));
        model.addAttribute("NORMAL_PRICE",settSrv.getValue("NORMAL_PRICE"));
        return "admin/settings";
    }

    @PostMapping("/admin/settings")
    public String settingsForm(@RequestParam("extraPrice") int extraPrice,
                               @RequestParam("normalPrice") int normalPrice,
                               Model model){
        settSrv.saveValue("EXTRA_PRICE", String.valueOf(extraPrice));
        settSrv.saveValue("NORMAL_PRICE", String.valueOf(normalPrice));
        return "admin/settings";
    }



}
