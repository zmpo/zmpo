package pl.kino.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.kino.model.User;
import pl.kino.service.UserService;

import javax.validation.Valid;
import java.security.Principal;

/**
 * Created by jakub on 26.05.17.
 */
@Controller
public class UserController {


    private UserService userService;

    @Autowired
    public UserController(UserService userService){
        this.userService=userService;
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("user", new User());
        return "register";
    }

    @PostMapping("/register")
    public String addUser(@ModelAttribute @Valid User user,
                          BindingResult bindResult) {
        if(bindResult.hasErrors() || !userService.isUserExist(user))
            return "register";
        else {
            userService.addWithDefaultRole(user);
            return "redirect:/login";
        }
    }

    @RequestMapping("/login")
    public String login(Principal user){
        if(user==null)
            return "login";
        else
            return "redirect:/";

    }

    @RequestMapping(value="/logout")
    public String logout() {
        SecurityContextHolder.getContext().getAuthentication().setAuthenticated(false);
        return "redirect:/";
    }
}
